PROJECT    = report
BIB        = --filter pandoc-citeproc --bibliography=references.bib
SRC        = $(PROJECT).md sections/introduction.md sections/implementation.md sections/conclusion.md
OPTIONS    = --template styles/template.tex --highlight-style tango

$(PROJECT):
	pandoc $(OPTIONS) $(BIB) -s $(SRC) -o $(PROJECT).pdf

clean:
	@echo "Cleaning..."
	@curl https://raw.githubusercontent.com/nelsonmestevao/spells/master/art/maid.ascii
	@rm $(PROJECT).pdf
	@echo "...✓ done!"
